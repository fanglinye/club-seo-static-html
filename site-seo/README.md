#### SEO静态化页面
* mustache
* koa


```

需求文档:

https://docs.google.com/document/d/1_wv_0g2fQYDAtR7BDwt0tbjC9MByecC5xrOzN_cVo6Y/edit?usp=sharing

会议纪要：

https://docs.google.com/document/d/1flW4Wx2tW0ZrSctHrPSzXHl3RfoZNoAGGsc5W0wyBds/edit#

 

淘宝静态seo页面：

https://www.taobao.com/list/product/%E5%AE%BD%E6%9D%BE%E7%9F%AD%E8%A3%A4%E4%B8%A4%E4%BB%B6%E5%A5%97%E5%A5%B3.htm

```


### pc端跳转联动
* 头部header
    * Search Guest Order      
        * http://dev.clubfactory.com:8088/views/home.html?showType=searchGuestOrder
    * login   
        * http://dev.clubfactory.com:8088/views/home.html?showType=login
* 商品详情
    * add cart
                encodeURIComponent(JSON.stringify({
                    skuId: 2493713,
                    quantity: 1
                  }))
                http://dev.clubfactory.com:8088/views/product/detail.html?productId=388462&operateType=addCart&operateData=%7B%22skuId%22%3A2493713%2C%22quantity%22%3A1%7D
    * buy 
        * 带参数直接跳转到下单页