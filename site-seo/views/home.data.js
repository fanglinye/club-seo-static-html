"use strict";

var category = require('../mock/category.json');
var indexData = require('../mock/index.json');

module.exports = {
    name:"ghy",
    page:"home",
    category:true,
    banner:true,
    ajax:"",
    data:{
        ...category,
        ...indexData,
        banner:indexData.banners[0],
    }
}