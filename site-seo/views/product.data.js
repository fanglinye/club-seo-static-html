"use strict";

var category = require('../mock/category.json');
var indexData = require('../mock/index.json');
var productData = require('../mock/product.json');



//组装排序==
const values = productData.sort_info.values.map(name=>{
    if(name === productData.sort_info.display_value){
        return {name,cls:"active"};
    }
    return {name};
})
//排序类型数据结构：{name,cls}
productData.sort_info.values = values;


//组装子类目list
const cat_subs = productData.category_info.sub_category.map(item=>{
    if(item.category_id === productData.category_info.category_id){
     item.cls = "active";   
    }
    return item;
});

// 子类目数据结构：{name,category_id,cls}
productData.category_info.sub_category = cat_subs;


//当前类目
//类目数据结构 ：{name,category_id,cls}
const categoryId = 53;
const top_category_info = category.top_category_info.map(item=>{
    item.cls = item.category_id === categoryId?'active':'';
    return item;
});


module.exports = {
    name:"ghy",
    page:"product",
    category:true,
    categoryId,
    sub:53,
    ajax:'?category_id='+categoryId,
    data:{
        top_category_info,
        values,
        ...indexData,
        ...productData,
    }
}