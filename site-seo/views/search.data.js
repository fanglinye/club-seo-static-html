"use strict";

var category = require('../mock/category.json');
var indexData = require('../mock/index.json');

module.exports = {
    name:"ghy",
    page:"search",
    ajax:'/search?query=bag',
    data:{
        ...category,
        ...indexData,
    }
}