"use strict";

var category = require('../mock/m/category.json');
var indexData = require('../mock/m/index.json');
var trendData = require('../mock/m/trend.json');


module.exports = {
    name: "ghy",
    page: "home",
    ajax:"",
    data: {
        ...category,
        ...indexData,
        ...trendData,
        banner:indexData.banners[0],
        list:indexData.product_info,
    }
}