"use strict";

var detailData = require('../mock/m/detail.json');
var reviewData = require('../mock/m/review.json');
var relatedData = require('../mock/m/related.json');

const {c_platform_price,list_price} = detailData.product_info;

detailData.product_info.list_price = list_price.toFixed(2);

//评价星星
const ratNum = Math.floor(detailData.rating);
detailData.rateStars = [1,2,3,4,5].map(i=>{
    return {star:i<=ratNum};
});

//折扣数据处理
detailData.off = Math.ceil(((c_platform_price - list_price) / c_platform_price).toFixed(2) * 100);


module.exports = {
    name: "ghy",
    page: "detail",
    data: {
        ...detailData,
        ...reviewData,
        ...relatedData,
        list:relatedData.product_list,
    }
}