"use strict";

var category = require('../mock/category.json');
var detailData = require('../mock/detail.json');
var relatedData = require('../mock/related.json');
var reviewData = require('../mock/review.json');

//是否展示图片轮播按钮 true/false
const showSilde = detailData.product_info.product_images.length>=5 ; 


const {c_platform_price,list_price} = detailData.product_info;

//商品详情，取小数点后两位
detailData.product_info.list_price = list_price.toFixed(2);


//评价星星，数据结构，固定数组长度5: [{star:true},{star:false},.....]
const ratNum = Math.floor(detailData.rating);
detailData.rateStars = [1,2,3,4,5].map(i=>{
    return {star:i<=ratNum};
});

//折扣数据处理：off，百分比整数
detailData.off = ((c_platform_price - list_price) / c_platform_price).toFixed(2) * 100


//sku数据处理 for choose
//数据格式： [{name,list: [{attr_value_id: attr_value_name}]}]
const skuMap = {};
detailData.sku_info.forEach(item=>{
    item.attribute_info.forEach(attr=>{
        if(!skuMap[attr.attr_id]){
            skuMap[attr.attr_id] = {name:attr.attr_name,attr_map:{},attr_list:[]}
        }
        skuMap[attr.attr_id].attr_map[attr.attr_value_id] = attr;
    });
});
const sku_list = [];
for(let k in skuMap){
    for(let i in skuMap[k].attr_map){
        skuMap[k].attr_list.push(skuMap[k].attr_map[i]);
    }
    sku_list.push(skuMap[k]);
}
detailData.sku_list = sku_list;


//评论数据处理
//数据结构：
// item:[{rateStars:[],user_name, comment_html, user_n:name,skuValStr:',',update_at:stringTime}]
//评价星星，数据结构，固定数组长度5: [{star:true},{star:false},.....]
const rList=reviewData.review_list.map(item=>{
    const {user_name,rating,sku,update_at} = item;
    //获取首个字母
    item.user_n = user_name.split('')[0];
    item.rateStars = [1,2,3,4,5].map(i=>{
        return {star:i<=rating};
    });
    const skuVals = sku.attribute_info.map(item=>{
        return item.attr_value_name;
    });
    item.skuValStr = skuVals.join(",");
    item.update_at = new Date(update_at).toLocaleString();
    return item;
});
reviewData.review_list = rList;


//当前类目
//类目数据结构 ：{name,category_id,cls}
const categoryId = 53;
const top_category_info = category.top_category_info.map(item=>{
    item.cls = item.category_id === categoryId?'active':'';
    return item;
});

const product_id = detailData.product_info.id;

module.exports = {
    name:"ghy",
    page:"detail",
    categoryId,
    category:true,
    showSilde,
    ajax:'/search/related?product_id='+product_id,
    data:{
        top_category_info,
        ...detailData,
        ...relatedData,
        ...reviewData,
    }
}