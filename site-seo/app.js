const path = require('path');
const koaMustache = require('koa-mustache');
const logger = require('koa-logger');
const Koa = require('koa');
const app = module.exports = new Koa();

const rq = require('async-request');

// setup views, appending .ejs
// when no extname is given to render()
app.use(logger());
app.use(koaMustache(__dirname + '/views', {
  extension: 'html',
  debug:true,
  partialsDir:'/mods'
}))

//定义子路由
const Router = require('koa-router');
const api = new Router()
const HOST = "https://www.clubfactory.com"
api.get('/product', async ( ctx )=>{
  const {request} = ctx;
  const resp = await rq(HOST + request.url);
  ctx.body = JSON.parse(resp.body||'{}');
})

api.get("/product/search",async ( ctx )=>{
  const {request} = ctx;
  const resp = await rq(HOST + request.url);
  ctx.body = JSON.parse(resp.body||'{}');
})

// 装载所有子路由
const router = new Router()
router.use('/v1', api.routes())

const related = new Router();
related.get("/product/search/related",async ( ctx )=>{
  const {request} = ctx;
  const resp = await rq(HOST + request.url);
  ctx.body = JSON.parse(resp.body||'{}');
});
router.use('/v3', related.routes())


// 加载路由中间件
app.use(router.routes());

console.log("server start : 3000");

if (!module.parent) app.listen(3000);
